# Changelog

## Release 42.alpha0 - 2022.04.07

### Removed:

All widgets have been removed for now.

### Updated:

Marble is now compiled against Gtk 4.

## Release 1.3.0 - 2020.07.06

### Removed:

- `PrettyWindow` was completely removed. Use libhandy's `HdyWindow` instead
(closes #1 #2 #3 #4 #5)
- Removed desktop menu entries (fixes #9)

### Updated:

- `Utils.set_theming_for_data()` now has a default value of `null` for it's
third parameter
- `-Dtest_side_window` was changed to `-Dtests`
- Updated flatpak SDK to `org.gnome.Sdk//3.36`
- New tests -- we now have one window that will contain a demo for every class
in the library (just like gtk4-demo)

### Internal:

- New build system files
- New Makefile scripts for building flatpak dev builds
