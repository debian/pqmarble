<div align="center">
  <h1><img src="./resources/com.raggesilver.PQMarble.svg" height="64"/>PQMarble</h1>
  <h4>Utility library for GNOME apps.</h4>
  <p>
    <a href="#install">Install</a> •
    <a href="./CHANGELOG.md">Changelog</a> •
    <a href="./COPYING">License</a>
  </p>
</div>

# Install

```bash
git clone https://gitlab.gnome.org/raggesilver/marble
cd marble
meson _build --prefix=/usr
ninja -C _build
sudo ninja -C _build install
```

If you ever want to remove it just

```bash
sudo ninja -C _build uninstall
```

**Dependencies**

- meson
- gtk4
